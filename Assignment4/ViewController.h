//
//  ViewController.h
//  Assignment4
//
//  Created by Jason Clinger on 2/4/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "secondViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    
    NSMutableArray* array;
}


@end

