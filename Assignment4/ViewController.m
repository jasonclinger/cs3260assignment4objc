//
//  ViewController.m
//  Assignment4
//
//  Created by Jason Clinger on 2/4/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "ViewController.h"
#import "secondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //UIBarButtonItem* addButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
    //target:self action:@selector(addItem:)];
    //self.navigationItem.rightBarButtonItem = addButton;
    
    //this commented out array ties it to the "path" and prepopulates it from the plist
    //NSString* path = [[NSBundle mainBundle]pathForResource:@"PropertyList" ofType:@"plist"];
    //array = [[NSMutableArray alloc]initWithContentsOfFile:path];
    
    array = [NSMutableArray new];
    NSLog(@"%@", array);
    
    myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [self.view addSubview: myTableView];
    
    //Monday commented out
    //NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    //[defaults setObject:@"Opening entry" forKey:@"world"];
    //[defaults synchronize];
    //Monday commented out
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadData:) name:@"reload" object:nil];
    
}

////testm
//-(void)fixData:(NSNotification*)notification{
//    [array removeObjectAtIndex:[[notification.userInfo objectForKey:@"index"] integerValue]];
//    [array insertObject:[notification.userInfo objectForKey:@"Info"] atIndex:[[notification.userInfo objectForKey:@"index"] integerValue]];
//    [myTableView reloadData];
//}
////testm

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {return array.count;
}

-(void)reloadData:(NSNotification*)n{
    //get new data from user defaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //[defaults setObject:@"journalEntry" forKey:@"journalEntry"];
    //[defaults setObject:@"title2" forKey:@"title2"];
    //[defaults synchronize];
    
    array = [[defaults objectForKey:@"journalEntries"] mutableCopy];
    
    //reload tableview
    [myTableView reloadData];
    
    NSLog(@"Saving journal entry, reloading data");
}

//describes what the cell will do and look like, returns and object, UITableViewCell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell){
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* dictionary = [array objectAtIndex:indexPath.row];
    cell.textLabel.text = dictionary[@"title"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"rowTouched" sender:indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"rowTouched"]){
        NSIndexPath* ip = (NSIndexPath*)sender;
    
    //testm
//    if ([segue.identifier isEqualToString:@"test1"]) {
//        secondViewController* svc = (secondViewController*)segue.destinationViewController;
//        svc.info = @{@"entry":@"",
//                     @"title":@"",};
//        svc.index = [NSNumber numberWithInteger:ip.row];
//    }
    //else{
    //testm
        
        
        
//    //Delegate method
//    if ([segue.identifier isEqualToString:@"showChildView"]) {
//        secondViewController* dsvc = segue.destinationViewController;
//        dsvc.delegate = self;
//        
//    }
//    
//    //end delegate method
    
        secondViewController* svc = (secondViewController*)segue.destinationViewController;
        svc.info = [array objectAtIndex:ip.row];
        svc.index = [NSNumber numberWithInteger:ip.row];
    
    //}  testm
    
    }

}

-(void)addItem:(UIBarButtonItem*)btn{
    
    secondViewController* svc = [secondViewController new];
    
    NSLog(@"addItem");
    
    [self.navigationController pushViewController:svc animated:YES];
}



//- (IBAction)btnTouchedAdd:(UIButton *)sender {
//    //// A4
//    
//    //NSIndexPath* ip = (NSIndexPath*)sender;
//    //secondViewController* svc = [[secondViewController alloc]init];
//    
//    NSLog(@"btnTouchedAdd");
//    [self performSegueWithIdentifier:@"test1" sender:nil];
//    
//    //[self.navigationController pushViewController:svc animated:YES];
//}



//- (IBAction)btnTouchedAdd:(UIBarButtonItem *)sender {
//    //NSIndexPath* ip = (NSIndexPath*)sender;
//    secondViewController* svc = [[secondViewController alloc]init];
//    
//    NSLog(@"btnTouchedAdd");
//    //[self performSegueWithIdentifier:@"rowTouched" sender:nil];
//    
//    [self performSegueWithIdentifier:@"rowTouched" sender:nil];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
