//
//  secondViewController.m
//  Assignment4
//
//  Created by Jason Clinger on 2/4/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "secondViewController.h"


@interface secondViewController ()

@end

@implementation secondViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor grayColor];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title2.text = [self.info objectForKey:@"title"];
    self.journalEntry.text = [self.info objectForKey:@"entry"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)SaveBtnTouched:(id)sender {
    //Monday set object to array
    //self.info = [array objectAtIndex: 0];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray* arrayOfJournalEntries = [[defaults objectForKey:@"journalEntries"] mutableCopy];
    
    //[defaults setObject:@"Saving entry test" forKey:@"world"];
    //[defaults synchronize];
    
    if (!arrayOfJournalEntries){
        arrayOfJournalEntries = [NSMutableArray new];
        NSLog(@"adding new entry");
        
//        [arrayOfJournalEntries replaceObjectAtIndex:[self.index integerValue] withObject:@{@"title":_title2.text, @"entry":_journalEntry.text}];
//        [defaults setObject:arrayOfJournalEntries forKey:@"journalEntries"];
//        [defaults synchronize];
//        NSLog(@"updating entry");
    
    }

    [arrayOfJournalEntries replaceObjectAtIndex:[self.index integerValue] withObject:@{@"title":_title2.text, @"entry":_journalEntry.text}];
    [defaults setObject:arrayOfJournalEntries forKey:@"journalEntries"];
    [defaults synchronize];
    NSLog(@"updating entry");
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"reload" object:nil];
    
//    //Start Testm
//    NSDictionary* information = @{@"entry":self.journalEntry.text,
//                                  @"title":self.title2.text,};
//    
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"LetThemKnow" object:nil userInfo:@{@"Info":information,
//                              @"index":self.index,}];
//    //End TestM
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
